import React from 'react'

import Utils       from './Utils.jsx'

class DateChanger extends React.Component {
    
    constructor(props) {
        super(props);

        // This binding is necessary to make 'this' work in the callback
        this.decrementDay = this.decrementDay.bind(this);
        this.incrementDay = this.incrementDay.bind(this);
    }
    
    decrementDay() {
        this.props.onDateChange(-1);
    }
    
    incrementDay() {
        this.props.onDateChange(+1);
    }
    
    render() {
        const datestring = Utils.date2string(this.props.date, 'd.m.yyyy');
        return (
            <div>
                <img src="img/player_back.png" className="imgLink fLeft" onClick={this.decrementDay} alt="Předchozí den" />
                <div className="selectedDate">{datestring}</div>
                <img src="img/player_forward.png" className="imgLink fLeft" onClick={this.incrementDay} alt="Následující den" />                
            </div>
        )
    }
}

export default DateChanger