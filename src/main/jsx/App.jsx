import React from 'react'
import ReactDOM from 'react-dom'
import axios from 'axios'

import Consts      from './Consts.jsx'
import DateChanger from './DateChanger.jsx'
import Modal       from './Modal.jsx'
import PubMenu     from './PubMenu.jsx'
import Settings    from './Settings.jsx'
import Utils       from './Utils.jsx'

class App extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
          date: Utils.getInitDate(),
          pubs: Utils.getCachedPubs(),
          layoutColumns: Utils.getLayoutColumns(),
          settingsModalIsOpen: false,
          aboutModalIsOpen: false
        };
        
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleShowSettingsModal = this.handleShowSettingsModal.bind(this);
        this.handleShowAboutModal = this.handleShowAboutModal.bind(this);
        this.handleHideModals = this.handleHideModals.bind(this);
        this.handleSettingsChange = this.handleSettingsChange.bind(this);
    }
    
    componentDidMount() {        
        // asynchronous load pubs from server
        axios.get(Consts.getApiBaseUrl() + 'pubs.json')
        .then(response =>  {
            if(response && response.status && response.status === 200 && response.data && response.data.pubs) {
                // //////////////////////////////////////////////////////////////////////////////////////////////////////
                // Check if there is a new pub -> if so, update localStorage/cache and application "pubs" status field //
                // //////////////////////////////////////////////////////////////////////////////////////////////////////
                var requireUpdate = false;
                var serverPubs = response.data.pubs;
                var localPubs = Utils.getCachedPubs();
                serverPubs.forEach(function(serverPub) {
                    var foundInLocal = false;
                    localPubs.forEach(function(localPub) {
                        if(localPub.code === serverPub.code) {
                            foundInLocal = true;                            
                        }
                    });                    
                    if(!foundInLocal) {
                        requireUpdate = true;
                        serverPub.displayOrder = serverPub.defaultDisplayOrder;
                        localPubs.push(serverPub);                        
                    }
                });
                if(requireUpdate) {
                    Utils.sortAndSaveCachedPubs(localPubs);
                    this.setState({
                        pubs: Utils.getCachedPubs() // really use cached array and call the getter, because there can be fixed override by URL parameters 
                    });
                }
            }
        })
        .catch(function (error) {
            window.console && console.log(error);
        });
    }
    
    render() {
        const manuBoxWidthClass = 'menuBox menuBoxWidth' + this.state.layoutColumns;
        const listPubs = this.state.pubs.map((pub, index) => 
            <div key={pub.code} className={manuBoxWidthClass}>
                <PubMenu pub={pub} date={this.state.date} />
            </div>
        );
        
        return (
            <div>
                <div className="headerLine">
                    <div className="headerContent">
                        <span className="headerLabel">jídeláky</span>
                        <img className="headerImg" src="img/plate_fork_64.png" alt="logo" />
                        {
                            (!Utils.isUrlJidelakyInfo())
                            &&
                            (<span className="headerLabel" style={{textDecoration: 'line-through'}} title="Nový odkaz je https://jidelaky.info - lépe se pamatuje a zadává do mobilu">ncc</span>)
                        }
                        {
                            (Utils.isUrlJidelakyInfo())
                            &&
                            (<span className="headerLabel">info</span>)
                        }                                                
                        <div className="dateIncWrap">
                            <DateChanger date={this.state.date} onDateChange={this.handleDateChange} />
                        </div>
                        <div className="buttonsWrap">
                          {
                            /*
                              <img id="allPubsMarkersButton" src="img/map_marker.png" className="imgLink" alt="Restaurace na mapě" title="Restaurace na mapě" />
                              <img id="settingsButton" src="img/gear_wheel.png" className="imgLink" alt="Nastavení" title="Nastavení" />
                              <img id="aboutButton" src="img/about.png" className="imgLink" style={{marginLeft:'7px'}} alt="O aplikaci" title="O aplikaci" />
                             */
                          }
                            <img onClick={this.handleShowSettingsModal} src="img/gear_wheel.png" className="imgLink" alt="Nastavení" title="Nastavení" />
                            <img onClick={this.handleShowAboutModal} src="img/about.png" className="imgLink" id="aboutButton" alt="O aplikaci" title="O aplikaci" />  
                        </div>
                    </div>
                </div>
                
                <div className="content">
                    {listPubs}
                </div>
                
                <Modal isOpen={this.state.aboutModalIsOpen} onClose={this.handleHideModals}>
                    <h1>Jídeláky NCC</h1>
                    <h2>Odpovědnost</h2>
                    <p>
                      Aplikace je bez garance dostupnosti, která závisí na infrastruktuře Google Cloud Platform. Autor se dále zříká:
                    </p>
                    <ul>
                      <li>Odpovědnosti za gramatické chyby v jídelních lístcích poskytovaných restauracemi.</li>
                      <li>Odpovědnosti za reálnou změnu menu oproti jídelním lístkům.</li>
                      <li>Odpovědnosti za včasné i věcně správné načítání textů.</li>
                      <li>Odpovědnosti za kvalitu připravených jídel.</li>
                      <li>Odpovědnosti za kompatibilitu s IE.</li>
                    </ul>

                    <h2>Technická realizace</h2>
                    <p>
                      Frontend – ReactJS - opensource, zdrojové kódy <a href="https://bitbucket.org/RadekBaxa/jidelakynccreact" target="_frontend-code">https://bitbucket.org/RadekBaxa/jidelakynccreact</a> <br/>
                      Backend – Google App Engine (Java, Spring, JSOAP, POI...)
                    </p>

                    <div style={{fontSize:'10px', float:'right'}}>autor <a href="http://www.radekbaxa.cz">Radek Baxa</a></div>
                    <div>&nbsp;</div>
                </Modal>
                    
                <Modal isOpen={this.state.settingsModalIsOpen} onClose={this.handleHideModals}>
                    <Settings onSettingsChange={this.handleSettingsChange} />
                </Modal>
                    
            </div>    
        );
    }
    
    handleShowSettingsModal() {
        this.setState({
            settingsModalIsOpen: true
        });
    }
    
    handleShowAboutModal() {
        this.setState({
            aboutModalIsOpen: true
        });
    }
    
    handleHideModals() {
        this.setState({
            settingsModalIsOpen: false,
            aboutModalIsOpen: false
        });
    }
    
    handleDateChange(daysForAdd) {
        this.setState(prevState => {
            return {
              date: Utils.addDay(prevState.date, daysForAdd)
            }
        });
    }
    
    handleSettingsChange() {
        this.setState({
            pubs: Utils.getCachedPubs(),
            layoutColumns: Utils.getLayoutColumns() // really use cached value and call the getter, because there can be fixed override by URL parameters
        });
    }
    
}

ReactDOM.render(
    <App />,
    document.getElementById('app')
)

