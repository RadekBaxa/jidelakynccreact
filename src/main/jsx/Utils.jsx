
const Utils = {
    
    addDay(date: Date, daysForAdd: number) {
        var newDate = new Date(date);
        newDate.setDate(newDate.getDate() + daysForAdd);
        return newDate;
    },
    
    date2string(date: Date, format: string) {
        // TODO this is fast super shit, rewrite it!
        
        format = format.toLowerCase();
        switch (format) {
        	case 'd-m-yyyy':
        		return date.getDate()  + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
        	case 'd.m.yyyy':
        	    return date.getDate()  + "." + (date.getMonth() + 1) + "." + date.getFullYear();
        	case 'dd-mm-yyyy':
        	    let day = date.getDate();
        	    let month = (date.getMonth() + 1);
                return (day < 10 ? ('0' + day) : day) + "-" + (month < 10 ? ('0' + month) : month) + "-" + date.getFullYear();
        	default:
        		throw 'unsupported format';
        }
    },
    
    sortAndSaveCachedPubs(pubs: Array) {
        // sort again by displayOrder (can be different from array ordering versus server suggested value defaultDisplayOrder)
        pubs.sort(function(a, b){return a.displayOrder - b.displayOrder}); 
        localStorage.setItem("pubs", JSON.stringify(pubs));
    },
    
    getCachedPubs() {
        // try to return value from local storage
        let cachedPubs = localStorage.getItem("pubs");
        if(cachedPubs && cachedPubs !== null && cachedPubs.length > 0) {
            try {
                let cachedPubsArray = JSON.parse(cachedPubs);
                if(Array.isArray(cachedPubsArray)) { // Important check because legacy version has cached JSON with one object "pubs"
                    
                    ////////////////////////////////////////////////////////////////////
                    // try to return overridden pubs (order and only selected values) //
                    ////////////////////////////////////////////////////////////////////
                    let urlPubsParam = this.getURLParameter("pubs");
                    if(urlPubsParam !== null) {
                        let overridenPubs = [];
                        let urlPubs = urlPubsParam.split(/,|;/g); // split pubs codes by comma or semicolon
                        urlPubs.forEach(function(urlPubCode) {
                            cachedPubsArray.forEach(function(cachedPub) {
                                if(urlPubCode === cachedPub.code) {
                                    overridenPubs.push(cachedPub);
                                }
                            });    
                        });
                        return overridenPubs;
                    }   
                    
                    // no URL pubs parameter => standard return cached pubs array
                    return cachedPubsArray;                    
                }
            } catch(err) {
                window.console && console.log('Error parse cached pubs! Error message:' + err.message);
            }
        }
        
        // return default = empty array
        return [];
    },
    
    getLayoutColumns() {
        ///////////////////////////////////////////////////////
        // try to return overridden value from URL parameter //
        ///////////////////////////////////////////////////////
        let urlLayoutParam = this.getURLParameter("layout");
        if(urlLayoutParam !== null) {
            try {
                let layoutInt = parseInt(urlLayoutParam); 
                if(layoutInt === 2 || layoutInt === 3 || layoutInt === 4) {
                    return layoutInt;
                } 
            } catch(err) {
                window.console && console.log('Error parse layout from URL param! Error message:' + err.message);
            }
        }
        
        ////////////////////////////////////////////
        // try to return value from local storage //
        ////////////////////////////////////////////
        let layoutColumns = localStorage.getItem("settings-layout-columns");
        if(layoutColumns !== null) {
            try {
                return parseInt(layoutColumns);
            } catch(err) {
                window.console && console.log('Error parse settings-layout-columns! Error message:' + err.message);
            }
        }
        
        // return default = 3 columns layout
        return 3;
    },
    
    setLayoutColumns(columns: number) {
        if([2, 3, 4].includes(columns)) {
            localStorage.setItem("settings-layout-columns", String(columns));
        }
    },
    
    getInitDate() {
        try {
            var hash = window.location.hash;
            if(hash && hash !== null && hash.length == 11) {
                var year = parseInt(hash.substr(hash.length - 4, 4));
                var month = parseInt(hash.substr(hash.length - 7, 2));
                var day = parseInt(hash.substr(hash.length - 10, 2));                                
                if(!isNaN(year) && !isNaN(month) && !isNaN(day)) {
                    if(1 <= month && month <= 12) {
                        if(1 <= day && day <= 31) {
                            return new Date(year, month - 1, day, 11, 55, 0, 0);
                        }
                    }                    
                }
            }
        } catch(err) {
            
        }
        return new Date();
    }, 
    
    getURLParameter(name) {
        let querystring = location.search;             
        return decodeURI((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(querystring) || [null, ''])[1].replace(/\+/g, '%20')) || null;
    },
    
    isUrlJidelakyInfo() {
        let hostname = location.hostname;
        return hostname.search(/jidelaky\.info/) >= 0;
    }
    
}

export default Utils