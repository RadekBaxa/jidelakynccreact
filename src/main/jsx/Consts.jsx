
const Consts = {
    
    getApiBaseUrl() {
        var hostName = window.location.hostname;
        if(hostName === 'jidelaky.info') {
            return 'https://jidelaky.info/api/';
        } else if(hostName === 'jidelakyncc.appspot.com') {
            return 'https://jidelakyncc.appspot.com/api/';
        } else {
            return 'https://jidelakyinfo.appspot.com/api/';
        }
    }    
}

export default Consts