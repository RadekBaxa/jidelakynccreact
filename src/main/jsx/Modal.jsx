import React from 'react'

class Modal extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            wrapElementId: 'modalId-' + Math.random().toString(36).substr(2, 9)
        };
        
        this.windowClickListener = this.windowClickListener.bind(this);
    }
    
    componentDidMount() {
        window.addEventListener("click", this.windowClickListener);
    }
    
    componentWillUnmount() {
        window.removeEventListener("click", this.windowClickListener);
    }
    
    windowClickListener(event) {
        var modalDiv = document.getElementById(this.state.wrapElementId);
        if (event.target == modalDiv) {
            this.props.onClose();
        }
    }
    
    render() {        
        return (
            <div>
            {
                (this.props.isOpen)
                &&
                (
                    <div id={this.state.wrapElementId} className="modal" style={{display:'block'}}>
                        <div className="modal-content">
                            <span className="close" onClick={this.props.onClose}>&times;</span>
                            <div className="modal-body">
                                {this.props.children}
                            </div>
                        </div>
                    </div>
                )
            }    
            </div>
        );
    }
}    

export default Modal