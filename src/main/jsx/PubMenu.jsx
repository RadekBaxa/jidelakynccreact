import React from 'react'
import axios from 'axios'

import Consts      from './Consts.jsx'
import Utils       from './Utils.jsx'

class PubMenu extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            pubMenu: this.getCachedPubMenu(props.date)
        };
        
        // This binding is necessary to make 'this' work in the callback
        this.getPubMenuFromServer = this.getPubMenuFromServer.bind(this);
        this.getCachedPubMenu = this.getCachedPubMenu.bind(this);
    }
    
    componentWillReceiveProps(nextProps) {
        if(nextProps.date && nextProps.date !== null) {
            if(this.props.date && this.props.date !== null) {
                if(nextProps.date.getTime() !== this.props.date.getTime()) {
                    // date field in properties changed 
                    // -> immediate update state.pubMenu from cache && fire AJAX for download new PubMenuItems (and cache it for next day)
                    
                    this.setState({
                        pubMenu: this.getCachedPubMenu(nextProps.date)
                    });
                    
                    this.getPubMenuFromServer(nextProps.date);                   // AJAX/load PubMenu based on new props date
                    this.getPubMenuFromServer(Utils.addDay(nextProps.date, +1)); // AJAX/load PubMenu (for caching) based one next day (after new props date)
                }
            }
        }
    }
    
    componentDidMount() {        
        this.getPubMenuFromServer(this.props.date);
        this.getPubMenuFromServer(Utils.addDay(this.props.date, +1));
    }
    
    getPubMenuFromServer(date: Date) {
        const pub = this.props.pub;
        const datestring = Utils.date2string(date, 'dd-mm-yyyy');
        
        // asynchronous load PubMenu (for one day) from server
        axios.get(Consts.getApiBaseUrl() + 'menu/' + pub.code + '/' + datestring + '.json')
        .then(response =>  {
            if(response && response.status === 200 && response.data && response.data.code) {
                // update localStorage = cache it
                localStorage.setItem(this.getCacheStorageKey(date), JSON.stringify(response.data));
                
                // update state but only if current props date === required AJAX date 
                if(date.getTime() === this.props.date.getTime()) {
                    this.setState({
                        pubMenu: response.data
                    });
                } 
            }
        })
        .catch(function (error) {
            window.console && console.log(error);
        });
        
        return [];
    }
    
    render() {        
        var pub = this.props.pub;
        var menuItems = null;
        if(this.state.pubMenu != null) {
            menuItems = this.state.pubMenu.menuItems.map(function(menuItem, index) {
                return (
                    <tr key={index}>
                        <td className="menuPrefixTd" title={menuItem.prefix && menuItem.prefix !== null ? menuItem.prefix : ''}>{menuItem.prefix && menuItem.prefix !== null && menuItem.prefix.length > 0 ? menuItem.prefix.substring(0,1) + '.' : ''}</td>
                        <td>{menuItem.name && menuItem.name !== null ? menuItem.name : ''}</td>
                        <td className="menuPriceTd">{menuItem.price && menuItem.price !== null ? (menuItem.price.replace(/Kč|,-/gi, '').trim() + ',-')  : ''}</td>
                    </tr>
                );
            });
        }
        
        return (
            <div>
                <div className="menuHeader">
                    {pub.name}
                    <div className="menuHeaderImgLinks">
                        { 
                            (pub.sourceUrl && pub.sourceUrl !== null && pub.sourceUrl.length > 0) 
                            && 
                            (<img src="img/chain_20w.png" className="imgLink" onClick={()=> window.open(this.props.pub.sourceUrl, "_PubLink")} alt="odkaz" />)
                        }
                        {/* <img src="img/map_marker_20w.png" className="imgLink" /> */}                        
                    </div>
                </div>
                <div className="menuSeparator"></div>
                <div className="menuBody">                    
                    {                       
                        (this.state.pubMenu === null) 
                        &&
                        (<div> Loading... </div>)       
                    }    
                    {                       
                        (this.state.pubMenu !== null && this.state.pubMenu.infoMessage && this.state.pubMenu.infoMessage !== null && this.state.pubMenu.infoMessage.length > 0) 
                        &&
                        (<div>{this.state.pubMenu.infoMessage}</div>)       
                    }
                    {
                        (this.state.pubMenu != null && this.state.pubMenu.menuItems.length > 0) 
                        &&
                        (<table className="pubMenuTable"><tbody>{menuItems}</tbody></table>)
                    }
                </div>
            </div>
        );
    }
    
    getCachedPubMenu(date: Date) {
        let cachedMenuString = localStorage.getItem(this.getCacheStorageKey(date));
        if(cachedMenuString && cachedMenuString !== null && cachedMenuString !== '') {
            let cachedMenu = JSON.parse(cachedMenuString);
            return cachedMenu;
        }
        return null;
    }
    
    getCacheStorageKey(date: Date) {
        let datestring = Utils.date2string(date, 'dd-mm-yyyy');
        let storageKey = this.props.pub.code + '-' + datestring;
        return storageKey;
    }
}

export default PubMenu