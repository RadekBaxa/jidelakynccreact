import React from 'react'

import Utils       from './Utils.jsx'

class Settings extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
          pubs: Utils.getCachedPubs()
        };
        
        this.pubMenuUp = this.pubMenuUp.bind(this);
        this.pubMenuDown = this.pubMenuDown.bind(this);
        this.pubMenuMove = this.pubMenuMove.bind(this);       
    }
    
    render() {
        const pubs = this.state.pubs;
        const settingsWindowHeight = (150 + pubs.length * 30) + 'px'; 
        const listPubs = pubs.map((pub, index) => 
            <div key={pub.code} className="pubMenuOrderOrder">
                <span className="settingsPubName">{pub.name}</span>
                {
                    (0 < index) ? (<img src="img/arrow_up.png" onClick={() => this.pubMenuUp(pub.code)} className="imgLink" />) :  (<div style={{width: '24px', float: 'left'}}>&nbsp;</div>)
                }
                {
                    (index < pubs.length - 1) ? (<img src="img/arrow_down.png" onClick={() => this.pubMenuDown(pub.code)} className="imgLink" />) :  (null)
                }
                <span style={{clear:'both'}}>&nbsp;</span>
            </div>
        );
        
        return (
            <div style={{height: settingsWindowHeight}}>
                <div className="leftSideSettings" >
                    <h2>Pořadí zobrazení menu</h2>
                    {listPubs}
                </div>
                      
                <div className="rightSideSettings">
                    <h2>Počtu sloupců menu</h2>
                    <img onClick={() => this.setLayoutColumns(2)} src="img/layout_2.png" className="imgLink" alt="2 sloupce" />
                    <img onClick={() => this.setLayoutColumns(3)} src="img/layout_3.png" className="imgLink" alt="3 sloupce" />
                    <img onClick={() => this.setLayoutColumns(4)} src="img/layout_4.png" className="imgLink" alt="4 sloupce" />
                </div>
            </div>
        );
    }
    
    pubMenuUp(pubCode: string) {
        this.pubMenuMove(pubCode, -1);
    }
    
    pubMenuDown(pubCode: string) {
        this.pubMenuMove(pubCode, +1);
    }
    
    pubMenuMove(pubCode: string, increment: number) {
        // first of all - find current pub index and calculate new required pub index in pubs array
        var pubs = Utils.getCachedPubs();
        var currentPubIndex = pubs.length - 1; // default = max index
        pubs.forEach(function(pub, index) {
            if(pub.code === pubCode) {
                currentPubIndex = index;
            }
        });
        var newPubIndex = currentPubIndex + increment;
        
        // move pub in array UP (if increment == -1) or DOWN (if increment == +1)
        pubs.splice(newPubIndex, 0, pubs.splice(currentPubIndex, 1)[0]);
        
        // update property displayOrder based on new pubs array order
        pubs.forEach(function(pub, index) {
            pub.displayOrder = index;
        });
        
        // save pubs back to localStorage = cache
        Utils.sortAndSaveCachedPubs(pubs);
        
        // update state will force re-render / update
        this.setState({
            pubs: pubs
        });
        
        // let the App know about settings changed 
        this.props.onSettingsChange();
    }
    
    setLayoutColumns(columns: number) {
        Utils.setLayoutColumns(columns);
        
        // let the App know about settings changed 
        this.props.onSettingsChange();
    }
    
}

export default Settings